# customer-mad-lib

Generates silly customer scenarios for practicing presentations.

See https://evanstoner.gitlab.io/customer-mad-lib.

## Contributing

You can add to the narrative under [public/index.html](https://gitlab.com/evanstoner/customer-mad-lib/-/blob/master/public/index.html)
and add answers for the blanks at [public/main.js](https://gitlab.com/evanstoner/customer-mad-lib/-/blob/master/public/main.js).
