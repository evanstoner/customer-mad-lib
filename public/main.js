var data = {
    "orgSize": [
        "small",
        "mid-size",
        "huge"
    ],
    "orgType": [
        "defense contractor",
        "system integrator",
        "federal agency",
        "state agency"
    ],
    "orgOffices": [
        "one main office",
        "a handful of offices",
        "one central office and dozens of satellite offices",
        "dozens of independent offices"
    ],
    "classification": [
        "proprietary",
        "HIPAA",
        "secret",
        "top secret/air-gapped"
    ],
    "workloadLanguage": [
        "Java",
        ".NET Core",
        ".NET Framework (Windows)",
        "Python",
        "C++"
    ],
    "workloadPackaging": [
        "system packages",
        "standalone containers",
        "tarballs and scripts",
        "Helm charts",
        "Kubernetes manifests",
        "virtual machine images",
        "OpenShift pipelines"
    ],
    "cloudPosture": [
        "one data center",
        "a few data centers",
        "our data center and one cloud",
        "one cloud",
        "multiple clouds"
    ],
    "concern": [
        "increasing scalability",
        "reducing time to market",
        "keeping costs down",
        "cloud security",
        "the shrinking talent pool",
        "our understaffing",
        "changing software development trends",
        "our six-month release cycle",
        "high turnaround time on IT tickets",
        "constantly changing business rules"
    ],
    "name": [
        "Alec",
        "Ben",
        "Sebastian",
        "Troy",
        "Josh",
        "Meghan",
        "Sara",
        "Melissa",
        "Heather",
        "Kim"
    ],
    "persona": [
        "VP of technology",
        "program manager",
        "project manager",
        "security analyst",
        "senior sysadmin",
        "junior sysadmin",
        "senior developer",
        "junior developer",
        "IT architect",
        "enterprise architect",
        "network engineer",
        "storage engineer"
    ],
    "attitude": [
        "only knows of Red Hat because of RHEL",
        "wants to use community open source",
        "is excited about DevOps",
        "thinks DevOps is just about new tools",
        "is concerned about the security of open source",
        "was impressed by AWS's sales pitch last week",
        "is responsible for several legacy applications",
        "isn't sure how to get up to speed on new tech"
    ]
};

$(function() {
    $("span[data-blank]").each(function() {
        var options = data[$(this).attr("data-blank")];
        var choice = Math.floor(Math.random() * options.length);
        $(this).text(options[choice]);
        options.splice(choice, 1);
    });
});
